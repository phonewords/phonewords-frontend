module.exports = {
  webpack: {
    alias: {
      '@graphql': require(`path`).resolve(__dirname, 'src/graphql'),
      '@init': require(`path`).resolve(__dirname, 'src/initialization'),
      '@routes': require(`path`).resolve(__dirname, 'src/routes'),
      '@components': require(`path`).resolve(__dirname, 'src/components'),
      '@hooks': require(`path`).resolve(__dirname, 'src/hooks'),
      '@utils': require(`path`).resolve(__dirname, 'src/common/utils'),
      '@assets': require(`path`).resolve(__dirname, 'src/common/assets/'),
      '@config': require(`path`).resolve(__dirname, 'src/common/config'),
      '@theme': require(`path`).resolve(__dirname, 'src/common/theme')
    }
  }
};
