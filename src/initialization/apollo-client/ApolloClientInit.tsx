import React from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

interface IProps {
  children?: React.ReactNode;
}

type ApolloClientInitProps = IProps;

const client = new ApolloClient({
  uri: 'http://localhost:4000/',
  cache: new InMemoryCache()
});

export const ApolloProviderInit: React.FC<ApolloClientInitProps> = (props) => {
  return <ApolloProvider client={client}>{props.children}</ApolloProvider>;
};

ApolloProviderInit.displayName = 'ApolloClientInit';
