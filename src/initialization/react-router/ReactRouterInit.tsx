import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { routes } from '../../routes/routes';

interface Props {}

type ReactRouterInitProps = Props;

const router = createBrowserRouter(Object.values(routes));

export const ReactRouterInit: React.FC<ReactRouterInitProps> = (props) => {
  return <RouterProvider router={router} />;
};

ReactRouterInit.displayName = 'ReactRouterInit';
