import { createTheme, ThemeProvider } from '@mui/material';
import { lightTheme } from '@theme';
import React from 'react';

interface Props {
  children?: React.ReactNode;
}

type MuiThemeProps = Props;

const theme = createTheme(lightTheme);

export const MuiTheme: React.FC<MuiThemeProps> = (props) => {
  return <ThemeProvider theme={theme}>{props.children}</ThemeProvider>;
};

MuiTheme.displayName = 'MuiTheme';
