import React from 'react';
import * as sc from './phone.sc';
import { PhoneInteractable, PhoneKeyboard, PhoneScreen } from '@components';
import { phoneConfigMap } from '@config';
import {
  useFetchWords,
  useNumericString,
  usePhoneLayout,
  useSelectedPhoneName
} from './phone.hooks';

interface Props {}

type PhoneProps = Props;

export const Phone: React.FC<PhoneProps> = () => {
  const selectedPhoneName = useSelectedPhoneName();
  const phoneLayout = usePhoneLayout(selectedPhoneName);

  const [errorText, setErrorText] = React.useState('');
  const {
    numericString,
    appendToNumbericString,
    setNumbericStringWithValidation
  } = useNumericString(setErrorText);

  const { isLoading, onSubmit, suggestedWords } = useFetchWords(
    numericString,
    setErrorText
  );
  return (
    <sc.Container {...phoneLayout.screenContainer} key={selectedPhoneName}>
      <sc.PhoneContainer {...phoneLayout.phoneContainer}>
        <sc.PhoneImage src={phoneConfigMap[selectedPhoneName].image} />
        <PhoneScreen
          {...phoneLayout.phoneScreen}
          value={numericString}
          onChange={setNumbericStringWithValidation}
          errorText={errorText}
          isLoading={isLoading}
          suggestedWords={suggestedWords}
        />
        <PhoneInteractable
          {...phoneLayout.phoneInteractable}
          onSubmit={onSubmit}
        />
        <PhoneKeyboard
          {...phoneLayout.phoneKeyboard}
          phoneName={selectedPhoneName}
          onInteract={appendToNumbericString}
        />
      </sc.PhoneContainer>
    </sc.Container>
  );
};

Phone.displayName = 'Phone';
