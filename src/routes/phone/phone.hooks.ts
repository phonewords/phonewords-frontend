import {
  Dimensions,
  OffsetMatrix,
  phoneConfigMap,
  phoneContainerConfig,
  PhoneName
} from '@config';
import { useWordsLazyQuery } from '@graphql';
import { useDimensions } from '@hooks';
import {
  getPhoneHeight,
  getScale,
  getScaledDimensions,
  getScaledOffset,
  isPhoneName
} from '@utils';
import { upperFirst } from 'lodash';
import React from 'react';
import { useParams } from 'react-router-dom';

const NUMBER_REGEX = /^[2-9]*$/;

export const useSelectedPhoneName = () => {
  const phoneNameUrlParam = upperFirst(useParams().phoneName);
  const [selectedPhoneName, setSelectedPhoneName] = React.useState<PhoneName>(
    isPhoneName(phoneNameUrlParam)
      ? PhoneName[phoneNameUrlParam]
      : PhoneName.Nokia3310
  );
  React.useEffect(() => {
    if (isPhoneName(phoneNameUrlParam)) {
      setSelectedPhoneName(PhoneName[phoneNameUrlParam]);
    }
  }, [phoneNameUrlParam]);

  return selectedPhoneName;
};

export const useScale = (selectedPhoneName: PhoneName) => {
  const dimensions = useDimensions();
  const getPhoneScale = React.useCallback(
    () => getScale(selectedPhoneName, getPhoneHeight(dimensions.height)),
    [dimensions.height, selectedPhoneName]
  );
  const [currentPhoneScale, setCurrentPhoneScale] = React.useState(
    getPhoneScale()
  );

  React.useEffect(() => {
    setCurrentPhoneScale(getPhoneScale());
  }, [getPhoneScale]);
  const scaleOffset = React.useCallback(
    (offset: OffsetMatrix) => getScaledOffset(offset, currentPhoneScale),
    [currentPhoneScale]
  );
  const scaleDimensions = React.useCallback(
    (dimensions: Dimensions) =>
      getScaledDimensions(dimensions, currentPhoneScale),
    [currentPhoneScale]
  );
  return { scaleOffset, scaleDimensions, currentPhoneScale };
};

export const usePhoneLayout = (selectedPhoneName: PhoneName) => {
  const { layout: phoneLayout } = phoneConfigMap[selectedPhoneName];
  const { scaleOffset, scaleDimensions, currentPhoneScale } =
    useScale(selectedPhoneName);
  return {
    screenContainer: {
      minHeight: phoneContainerConfig.minHeight
    },
    phoneContainer: {
      heightPercentage: `${phoneContainerConfig.phoneHeigthPercentage * 100}%`
    },
    phoneScreen: {
      containerOffset: scaleOffset(phoneLayout.screenOffset)
    },
    phoneInteractable: {
      containerOffset: scaleOffset(phoneLayout.interactableOffset),
      submitDimensions: scaleDimensions(phoneLayout.submitDimensions),
      submitOffset: scaleOffset(phoneLayout.submitOffset)
    },
    phoneKeyboard: {
      containerOffset: scaleOffset(phoneLayout.keyboardOffset),
      columnOffsets: phoneLayout.keyColums.map((kC) => scaleOffset(kC)),
      columnWidth: phoneLayout.keyDimensions.width * currentPhoneScale.width,
      rowOffsets: phoneLayout.keyRows.map((kR) => scaleOffset(kR)),
      rowHeight: phoneLayout.keyDimensions.height * currentPhoneScale.height
    }
  };
};

export const useNumericString = (
  setErrorText: React.Dispatch<React.SetStateAction<string>>
) => {
  const [numericString, setNumericString] = React.useState('');
  const setNumbericStringWithValidation = React.useCallback(
    (input: string) => {
      if (NUMBER_REGEX.test(input)) {
        setErrorText('');
        setNumericString(input);
      } else {
        setErrorText('Only digits allowed. [2-9]');
      }
    },
    [setErrorText]
  );
  const appendToNumbericString = React.useCallback(
    (value: string) => {
      setNumbericStringWithValidation(`${numericString}${value}`);
    },
    [numericString, setNumbericStringWithValidation]
  );
  return {
    numericString,
    setNumbericStringWithValidation,
    appendToNumbericString
  };
};

export const useFetchWords = (
  numericString: string,
  setErrorText: React.Dispatch<React.SetStateAction<string>>
) => {
  const [fetchWords, { data, loading }] = useWordsLazyQuery({
    onError: (err) => {
      if (err.message === 'Argument Validation Error') {
        setErrorText(err.message);
      } else {
        setErrorText('An Error occured fetching words.');
      }
    }
  });

  const onSubmit = React.useCallback(() => {
    if (numericString !== '') {
      fetchWords({ variables: { numberInput: numericString } });
    }
  }, [fetchWords, numericString]);

  const suggestedWords = React.useMemo(() => {
    // if the id is cached, then the cached data could be used, instead of clearing
    // at some point, a cache clean would probably be a good idea
    if (data?.words.id !== numericString || !data?.words.payload) {
      return [];
    }
    return data?.words.payload;
  }, [data?.words.id, data?.words.payload, numericString]);

  return { onSubmit, suggestedWords, isLoading: loading };
};
