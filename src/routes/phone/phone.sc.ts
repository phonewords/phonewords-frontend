import { styled } from '@mui/material';

export const Container = styled('div')<{ minHeight: number }>((props) => ({
  flex: 1,
  minHeight: props.minHeight,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  [props.theme.breakpoints.down('xs')]: {
    width: props.theme.breakpoints.values.xs
  }
}));

export const PhoneImage = styled('img')({
  objectFit: 'contain',
  height: '100%'
});

export const PhoneContainer = styled('div')<{ heightPercentage: string }>(
  (props) => ({
    position: 'relative',
    height: props.heightPercentage
  })
);
