import { PhoneLink } from '@components';
import { PhoneName } from '@config';
import { generatePhonePath } from '@utils';
import React from 'react';
import { Outlet, useMatch, useNavigate } from 'react-router-dom';
import { AppRoutes } from '../routes';
import * as sc from './root.sc';

interface Props {}

type RootProps = Props;

export const Root: React.FC<RootProps> = () => {
  const navigate = useNavigate();
  const isValidRoute = useMatch(AppRoutes.PHONE.path);
  React.useEffect(() => {
    if (!isValidRoute) {
      navigate(generatePhonePath(PhoneName.Nokia3310));
    }
  });
  return (
    <sc.Container>
      <sc.Sidebar>
        <sc.HeaderText variant="h6">Phone select</sc.HeaderText>
        {Object.values(PhoneName).map((phoneName) => (
          <PhoneLink phoneName={phoneName} key={phoneName} />
        ))}
      </sc.Sidebar>
      <Outlet />
    </sc.Container>
  );
};

Root.displayName = 'Root';
