import { styled, Typography } from '@mui/material';

export const Container = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  height: '100vh',
  width: '100wh'
});

export const HeaderText = styled(Typography)({
  padding: '20px 30px'
});

export const Sidebar = styled('div')((props) => {
  return {
    display: 'flex',
    flexDirection: 'column',
    heigth: '100%',
    backgroundColor: props.theme.customColors.transparentGrey.opacity030
  };
});
