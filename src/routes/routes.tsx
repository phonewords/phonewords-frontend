import { RouteObject } from 'react-router-dom';
import { Phone } from './phone/Phone';
import { Root } from './root/Root';

export const AppRoutes = {
  ROOT: { path: '/', element: <Root /> },
  PHONE: { path: '/phone/:phoneName', element: <Phone /> }
};

export const routes: RouteObject[] = [
  {
    ...AppRoutes.ROOT,
    children: [AppRoutes.PHONE]
  }
];
