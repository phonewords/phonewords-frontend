import nokia3310 from '@assets/images/nokia-3310.png';
import nokia6610 from '@assets/images/nokia-6610.png';

export enum PhoneName {
  Nokia3310 = 'NOKIA_3310',
  Nokia6610 = 'NOKIA_6610'
}

export type OffsetMatrix = [number, number, number, number];

export interface Dimensions {
  height: number;
  width: number;
}

type KeyValueType = number | string;
type Column = Key[];
interface Key {
  primaryValue: KeyValueType;
}

export interface PhoneConfig {
  image: string;
  dimensions: Dimensions;
  layout: {
    keyboardOffset: OffsetMatrix;
    keyColums: [OffsetMatrix, OffsetMatrix, OffsetMatrix];
    keyRows: [OffsetMatrix, OffsetMatrix, OffsetMatrix, OffsetMatrix];
    keyDimensions: Dimensions;
    screenOffset: OffsetMatrix;
    interactableOffset: OffsetMatrix;
    submitDimensions: Dimensions;
    submitOffset: OffsetMatrix;
  };
  keyboardConfig: Column[];
}

const defaultKeyboardConfig = [
  [
    { primaryValue: 1 },
    { primaryValue: 4 },
    { primaryValue: 7 },
    { primaryValue: '*' }
  ],
  [
    { primaryValue: 2 },
    { primaryValue: 5 },
    { primaryValue: 8 },
    { primaryValue: 0 }
  ],
  [
    { primaryValue: 3 },
    { primaryValue: 6 },
    { primaryValue: 9 },
    { primaryValue: '#' }
  ]
];

export const phoneConfigMap: { [key in PhoneName]: PhoneConfig } = {
  [PhoneName.Nokia3310]: {
    image: nokia3310,
    dimensions: {
      height: 2160,
      width: 948
    },
    layout: {
      keyboardOffset: [1384, 103, 146, 107],
      keyColums: [
        [18, 28, 0, 19],
        [58, 0, 0, 20],
        [17, 5, 0, 50]
      ],
      keyRows: [
        [0, 0, 0, 0],
        [45, 0, 0, 0],
        [40, 0, 0, 0],
        [40, 0, 0, 0]
      ],
      keyDimensions: {
        height: 110,
        width: 200
      },
      screenOffset: [560, 150, 1130, 150],
      interactableOffset: [1120, 150, 770, 150],
      submitDimensions: {
        height: 100,
        width: 355
      },
      submitOffset: [0, 0, 0, 150]
    },
    keyboardConfig: defaultKeyboardConfig
  },
  [PhoneName.Nokia6610]: {
    image: nokia6610,
    dimensions: {
      height: 1776,
      width: 896
    },
    layout: {
      keyboardOffset: [1170, 170, 100, 200],
      keyColums: [
        [0, 0, 0, 10],
        [20, 80, 0, 80],
        [5, 10, 0, 0]
      ],
      keyRows: [
        [0, 0, 0, 0],
        [10, 0, 0, 0],
        [3, 0, 0, 0],
        [0, 0, 0, 0]
      ],
      keyDimensions: {
        height: 110,
        width: 125
      },
      screenOffset: [360, 170, 890, 200],
      interactableOffset: [935, 155, 100, 180],
      submitDimensions: {
        height: 250,
        width: 260
      },
      submitOffset: [0, 0, 0, 153]
    },
    keyboardConfig: defaultKeyboardConfig
  }
};

export const phoneContainerConfig = {
  phoneHeigthPercentage: 0.9,
  minHeight: 660
};
