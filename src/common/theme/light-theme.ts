import { ThemeOptions } from '@mui/material';

export const lightTheme: ThemeOptions = {
  customColors: {
    screenColor: 'rgba(96, 125, 85)',
    transparentGrey: {
      opacity020: 'rgba(167, 167, 168, 0.2)',
      opacity030: 'rgba(167, 167, 168, 0.3)',
      opacity040: 'rgba(167, 167, 168, 0.4)'
    },
    transparentBlue: {
      opacity020: 'rgba(49, 148, 211, 0.2)',
      opacity030: 'rgba(49, 148, 211, 0.3)',
      opacity040: 'rgba(49, 148, 211, 0.4)'
    }
  }
};
