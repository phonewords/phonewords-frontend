import {
  Theme as MuiTheme,
  ThemeOptions as MuiThemeOptions
} from '@mui/material/styles';

declare module '@mui/material/styles' {
  interface Theme extends MuiTheme {
    customColors: {
      screenColor: string;
      transparentGrey: {
        opacity020: string;
        opacity030: string;
        opacity040: string;
      };
      transparentBlue: {
        opacity020: string;
        opacity030: string;
        opacity040: string;
      };
    };
  }
  interface ThemeOptions extends MuiThemeOptions {
    customColors: {
      screenColor: string;
      transparentGrey: {
        opacity020: string;
        opacity030: string;
        opacity040: string;
      };
      transparentBlue: {
        opacity020: string;
        opacity030: string;
        opacity040: string;
      };
    };
  }
  export function createTheme(options?: ThemeOptions): Theme;
}
