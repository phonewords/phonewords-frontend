import { PhoneName } from '@config';
import { generatePath } from 'react-router-dom';
import { AppRoutes } from '../../routes/routes';

export const generatePhonePath = (phoneName: PhoneName) =>
  generatePath(AppRoutes.PHONE.path, {
    phoneName: phoneName.replace('_', '').toLowerCase()
  });
