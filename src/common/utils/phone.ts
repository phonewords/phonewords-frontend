import {
  Dimensions,
  OffsetMatrix,
  phoneConfigMap,
  phoneContainerConfig,
  PhoneName
} from '@config';
import { mapValues } from 'lodash';

export const getPhoneHeight = (height: number) =>
  (height > phoneContainerConfig.minHeight
    ? height
    : phoneContainerConfig.minHeight) *
  phoneContainerConfig.phoneHeigthPercentage;

const getAspectRatio = (phoneName: PhoneName) =>
  phoneConfigMap[phoneName].dimensions.width /
  phoneConfigMap[phoneName].dimensions.height;

export const getScale = (phoneName: PhoneName, height: number): Dimensions => ({
  height: height / phoneConfigMap[phoneName].dimensions.height,
  width:
    (height * getAspectRatio(phoneName)) /
    phoneConfigMap[phoneName].dimensions.width
});

export const getScaledOffset = (
  offset: OffsetMatrix,
  scale: Dimensions
): OffsetMatrix =>
  offset.map(
    (o, i) => o * scale[i % 2 === 0 ? 'width' : 'height']
  ) as OffsetMatrix;

export const getScaledDimensions = (
  dimensions: Dimensions,
  scale: Dimensions
): Dimensions =>
  mapValues(dimensions, (d, key) => d * scale[key as keyof Dimensions]);

export const isPhoneName = (name?: string): name is keyof typeof PhoneName =>
  name === undefined
    ? false
    : Object.keys(PhoneName)
        .map((pN) => pN.toString())
        .includes(name);
