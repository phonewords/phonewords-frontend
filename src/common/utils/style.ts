import { OffsetMatrix } from '@config';

type WhitespaceType = 'absolute' | 'margin' | 'padding';

export const offsetStyleMap = (
  offset: OffsetMatrix,
  whitespaceType: WhitespaceType
) => {
  switch (whitespaceType) {
    case 'absolute':
      return {
        top: offset[0],
        right: offset[1],
        bottom: offset[2],
        left: offset[3]
      };
    case 'margin':
      return { margin: `${offset.join('px ')}px` };
    case 'padding':
      return { padding: `${offset.join('px ')}px` };
    default:
      console.warn(
        '[OffsetStyleMap] - The whitespaceType used is not supported: ',
        whitespaceType
      );
      return {};
  }
};

export const getKeyInteractionColoring = (
  hover: string,
  active: string,
  ripple: string
) => ({
  '&:hover': {
    backgroundColor: hover
  },
  ':active': {
    backgroundColor: active
  },
  '.MuiTouchRipple-child': {
    backgroundColor: ripple
  }
});
