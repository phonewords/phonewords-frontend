import { styled, Button } from '@mui/material';
import { getKeyInteractionColoring } from '@utils';
import { Link } from 'react-router-dom';

export const PhoneLink = styled(Link)({
  textDecoration: 'none',
  color: 'unset',
  ':hover': {
    cursor: 'default'
  }
});

export const PhoneLinkButton = styled(Button)((props) => ({
  width: '100%',
  ...getKeyInteractionColoring(
    props.theme.customColors.transparentBlue.opacity020,
    props.theme.customColors.transparentBlue.opacity030,
    props.theme.customColors.transparentBlue.opacity040
  ),
  borderRadius: 0,
  color: props.theme.customColors.screenColor
}));
