import { PhoneName } from '@config';
import { generatePhonePath } from '@utils';
import React from 'react';
import * as sc from './phone-link.sc';

interface Props {
  phoneName: PhoneName;
}

type PhoneLinkProps = Props;

export const PhoneLink: React.FC<PhoneLinkProps> = (props) => {
  return (
    <sc.PhoneLink to={generatePhonePath(props.phoneName)}>
      <sc.PhoneLinkButton variant="text">
        {props.phoneName.replace('_', ' ')}
      </sc.PhoneLinkButton>
    </sc.PhoneLink>
  );
};

PhoneLink.displayName = 'PhoneLink';
