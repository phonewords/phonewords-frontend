import { OffsetMatrix } from '@config';
import { styled } from '@mui/material';
import { offsetStyleMap, getKeyInteractionColoring } from '@utils';
import { PhoneKey } from '../styled/phone-key.sc';

interface ContainerProps {
  offset: OffsetMatrix;
}

interface SubmitProps {
  offset: OffsetMatrix;
}

export const Container = styled('div')<ContainerProps>((props) => ({
  position: 'absolute',
  display: 'flex',
  ...offsetStyleMap(props.offset, 'absolute')
}));

export const Submit = styled(PhoneKey)<SubmitProps>((props) => ({
  ...getKeyInteractionColoring(
    props.theme.customColors.transparentBlue.opacity020,
    props.theme.customColors.transparentBlue.opacity030,
    props.theme.customColors.transparentBlue.opacity040
  )
}));
