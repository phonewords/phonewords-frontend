import { Dimensions, OffsetMatrix } from '@config';
import React from 'react';
import * as sc from './phone-interactable.sc';

interface Props {
  containerOffset: OffsetMatrix;
  submitDimensions: Dimensions;
  submitOffset: OffsetMatrix;
  onSubmit: () => void;
}

type PhoneInteractableProps = Props;

export const PhoneInteractable: React.FC<PhoneInteractableProps> = (props) => {
  return (
    <sc.Container offset={props.containerOffset}>
      <sc.Submit
        {...props.submitDimensions}
        offset={props.submitOffset}
        onClick={props.onSubmit}
      />
    </sc.Container>
  );
};

PhoneInteractable.displayName = 'PhoneInteractable';
