import { OffsetMatrix } from '@config';
import { styled } from '@mui/material';
import { offsetStyleMap } from '@utils';

interface ContainerProps {
  offset: OffsetMatrix;
}

interface ColumnProps {
  offset: OffsetMatrix;
  width: number;
}

export const Container = styled('div')<ContainerProps>((props) => ({
  display: 'flex',
  position: 'absolute',
  ...offsetStyleMap(props.offset, 'absolute')
}));

export const Column = styled('div')<ColumnProps>((props) => ({
  flexDirection: 'column',
  backgroundColor: 'rgba(100,100,100, 0.1)',
  ...offsetStyleMap(props.offset, 'margin'),
  width: props.width
}));
