import { PhoneName, phoneConfigMap, OffsetMatrix } from '@config';
import React from 'react';
import { PhoneKey } from '../styled/phone-key.sc';
import * as sc from './phone-keyboard.sc';

interface Props {
  phoneName: PhoneName;
  containerOffset: OffsetMatrix;
  columnOffsets: OffsetMatrix[];
  columnWidth: number;
  rowOffsets: OffsetMatrix[];
  rowHeight: number;
  onInteract: (prev: string) => void;
}

type PhoneKeyboardProps = Props;

export const PhoneKeyboard: React.FC<PhoneKeyboardProps> = (props) => {
  return (
    <sc.Container offset={props.containerOffset}>
      {phoneConfigMap[props.phoneName].keyboardConfig.map(
        (column, columnIndex) => (
          <sc.Column
            key={columnIndex}
            offset={props.columnOffsets[columnIndex]}
            width={props.columnWidth}
          >
            {column.map((keyValue, keyIndex) => (
              <PhoneKey
                key={keyValue.primaryValue}
                offset={props.rowOffsets[keyIndex]}
                height={props.rowHeight}
                onClick={() => props.onInteract(`${keyValue.primaryValue}`)}
              />
            ))}
          </sc.Column>
        )
      )}
    </sc.Container>
  );
};

PhoneKeyboard.displayName = 'PhoneKeyboard';
