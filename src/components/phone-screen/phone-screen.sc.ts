import { OffsetMatrix } from '@config';
import { LinearProgress, styled, TextField, Typography } from '@mui/material';
import { display } from '@mui/system';
import { offsetStyleMap } from '@utils';
import { ThemeConsumer } from 'styled-components';

interface ContainerProps {
  offset: OffsetMatrix;
}

export const Container = styled('div')<ContainerProps>((props) => ({
  position: 'absolute',
  flexDirection: 'column',
  display: 'flex',
  borderRadius: 12,
  backgroundColor: props.theme.customColors.screenColor,
  ...offsetStyleMap(props.offset, 'absolute'),
  padding: `5px 5px`
}));

export const Input = styled(TextField)({
  backgroundColor: 'transparent',
  width: '100%',
  '.MuiInputBase-input': { padding: 0 },
  '.MuiInputBase-formControl': {
    fontFamily: 'NokiaFc22'
  },
  '.MuiOutlinedInput-notchedOutline': {
    borderWidth: 0
  },
  '.Mui-focused': {
    '.MuiOutlinedInput-notchedOutline': {
      borderWidth: 0
    }
  }
});

export const ErrorText = styled(Typography)((props) => {
  return {
    fontFamily: 'NokiaFc22',
    marginTop: 5,
    marginBottom: 5,
    color: props.theme.palette.error.dark
  };
});

export const SuggestionsContainer = styled('div')({
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'auto'
});

export const SuggestionsLoading = styled(LinearProgress)((props) => ({
  backgroundColor: props.theme.palette.text.primary,
  '.MuiLinearProgress-barColorPrimary': {
    backgroundColor: props.theme.customColors.screenColor
  }
}));

export const Suggestions = styled(Typography)({
  width: '100%',
  fontFamily: 'NokiaFc22',
  justifyContent: 'center',
  alignItems: 'center'
});
