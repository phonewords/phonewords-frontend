import { OffsetMatrix } from '@config';
import { Words } from '@graphql';
import React from 'react';
import * as sc from './phone-screen.sc';

interface Props {
  containerOffset: OffsetMatrix;
  onChange: (value: string) => void;
  value: string;
  errorText: string;
  isLoading: boolean;
  suggestedWords: Words['payload'];
}

type PhoneScreenProps = Props;

export const PhoneScreen: React.FC<PhoneScreenProps> = (props) => {
  return (
    <sc.Container offset={props.containerOffset}>
      <sc.Input
        id="numericString"
        placeholder="Type a numeric string"
        value={props.value}
        onChange={(event) => props.onChange(event.target.value)}
      />
      {props.errorText !== '' && <sc.ErrorText>{props.errorText}</sc.ErrorText>}
      <sc.SuggestionsContainer>
        {/* MUI best practice is to only show loading after 1s has passed */}
        {props.isLoading && <sc.SuggestionsLoading />}
        {props.suggestedWords.length > 0 ? (
          <sc.Suggestions>{props.suggestedWords.join(', ')}</sc.Suggestions>
        ) : (
          <sc.Suggestions style={{ textAlign: 'center' }}>
            No suggestions yet.
          </sc.Suggestions>
        )}
      </sc.SuggestionsContainer>
    </sc.Container>
  );
};

PhoneScreen.displayName = 'PhoneScreen';
