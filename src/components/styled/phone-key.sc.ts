import { OffsetMatrix } from '@config';
import { Button, styled } from '@mui/material';
import { offsetStyleMap, getKeyInteractionColoring } from '@utils';

interface KeyButtonProps {
  offset?: OffsetMatrix;
  height?: number;
  width?: number;
  inheritwidth?: boolean;
}

export const PhoneKey = styled(Button)<KeyButtonProps>((props) => ({
  minWidth: 'unset',
  ...(props.height && { height: props.height }),
  ...(!props.width && { width: '100%' }),
  ...(props.width && { width: props.width }),
  ...(props.offset && offsetStyleMap(props.offset, 'margin')),
  ...getKeyInteractionColoring(
    props.theme.customColors.transparentGrey.opacity020,
    props.theme.customColors.transparentGrey.opacity030,
    props.theme.customColors.transparentGrey.opacity040
  )
}));
