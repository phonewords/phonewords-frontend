import { gql } from '@apollo/client';

export const words = gql`
  query words($numberInput: String!, $wordList: [String!]) {
    words(numberInput: $numberInput, wordList: $wordList) {
      id
      count
      payload
    }
  }
`;
