import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  __typename?: 'Query';
  words: Words;
};


export type QueryWordsArgs = {
  numberInput: Scalars['String'];
  wordList?: InputMaybe<Array<Scalars['String']>>;
};

export type Words = {
  __typename?: 'Words';
  count: Scalars['Float'];
  id: Scalars['ID'];
  payload: Array<Scalars['String']>;
};

export type WordsQueryVariables = Exact<{
  numberInput: Scalars['String'];
  wordList?: InputMaybe<Array<Scalars['String']> | Scalars['String']>;
}>;


export type WordsQuery = { __typename?: 'Query', words: { __typename?: 'Words', id: string, count: number, payload: Array<string> } };


export const WordsDocument = gql`
    query words($numberInput: String!, $wordList: [String!]) {
  words(numberInput: $numberInput, wordList: $wordList) {
    id
    count
    payload
  }
}
    `;

/**
 * __useWordsQuery__
 *
 * To run a query within a React component, call `useWordsQuery` and pass it any options that fit your needs.
 * When your component renders, `useWordsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWordsQuery({
 *   variables: {
 *      numberInput: // value for 'numberInput'
 *      wordList: // value for 'wordList'
 *   },
 * });
 */
export function useWordsQuery(baseOptions: Apollo.QueryHookOptions<WordsQuery, WordsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<WordsQuery, WordsQueryVariables>(WordsDocument, options);
      }
export function useWordsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<WordsQuery, WordsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<WordsQuery, WordsQueryVariables>(WordsDocument, options);
        }
export type WordsQueryHookResult = ReturnType<typeof useWordsQuery>;
export type WordsLazyQueryHookResult = ReturnType<typeof useWordsLazyQuery>;
export type WordsQueryResult = Apollo.QueryResult<WordsQuery, WordsQueryVariables>;